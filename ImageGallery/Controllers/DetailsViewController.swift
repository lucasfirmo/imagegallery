//
//  DetailsViewController.swift
//  ImageGallery
//
//  Created by Lucas Domene on 25/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, ZoomingViewController {
	
	@IBOutlet weak var imageView: UIImageView!

	var image: UIImage!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.largeTitleDisplayMode = .never
		title = Constants.detailsViewTitle
		imageView.image = image
	}
	
	func zoomingImageView(for transition: ZoomTransitionDelegate) -> UIImageView? {
		return imageView
	}
	
}
