//
//  PhotosViewController+CollectionView.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

extension PhotosViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	// MARK: - UICollectionViewDataSource
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return photos.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.photoCell, for: indexPath) as! PhotoCollectionViewCell
		cell.imageView.image = nil
		return cell
	}
	
	// MARK: - UICollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if !isOffline {
			paging(forIndexPath: indexPath)
		}
		if let photoCell = (cell as? PhotoCollectionViewCell) {
			self.loadImage(forCell: photoCell, atIndexPath: indexPath)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		selectedIndexPath = indexPath
		performSegue(withIdentifier: Constants.showDetailsSegue, sender: nil)
	}
	
	// MARK: - Utils
	
	func newIndexPaths(totalItems: Int) -> [IndexPath] {
		var indexPaths = [IndexPath]()
		for row in (self.photos.count - totalItems)..<self.photos.count {
			indexPaths.append(IndexPath(row: row, section: 0))
		}
		return indexPaths
	}
	
	private func paging(forIndexPath indexPath: IndexPath) {
		if indexPath.row == photos.count - 2 && !isLastPage {
			page += 1
			fetchPhotos()
		}
	}
	
	private func loadImage(forCell cell: PhotoCollectionViewCell, atIndexPath indexPath: IndexPath) {
		cell.startLoading()
		if let photo = photos[safe: indexPath.row] {
			cell.id = photo.id
			photoService.downloadImage(forPhoto: photo) { image in
				self.photoService.setImage(image, forKey: photo.id)
				if photo.id == cell.id {
					self.add(image: image, toCell: cell)
				}
			}
		}
	}
	
	private func add(image: UIImage?, toCell cell: PhotoCollectionViewCell) {
		DispatchQueue.main.async {
			cell.stopLoading()
			cell.imageView.alpha = 0.0
			cell.imageView.image = image
			UIView.animate(withDuration: 0.5) {
				cell.imageView.alpha = 1.0
			}
		}
	}
	
	// MARK: - UICollectionViewDelegateFlowLayout
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let layout = collectionViewLayout as! UICollectionViewFlowLayout
		let numberOfItemsPerRow: CGFloat = UIDevice.current.orientation == UIDeviceOrientation.portrait ? 2.0 : 3.0
		let itemWidth = (collectionView.bounds.width - layout.minimumLineSpacing - (numberOfItemsPerRow * layout.sectionInset.left)) / numberOfItemsPerRow
		return CGSize(width: itemWidth, height: itemWidth)
	}
	
}
