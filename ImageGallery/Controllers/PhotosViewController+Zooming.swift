//
//  PhotosViewController+Zooming.swift
//  ImageGallery
//
//  Created by Lucas Domene on 25/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

extension PhotosViewController: ZoomingViewController {
	
	func zoomingImageView(for transition: ZoomTransitionDelegate) -> UIImageView? {
		if let indexPath = selectedIndexPath {
			let cell = collectionView.cellForItem(at: indexPath) as! PhotoCollectionViewCell
			return cell.imageView
		}
		return nil
	}
	
}
