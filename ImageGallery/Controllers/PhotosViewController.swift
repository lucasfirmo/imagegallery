//
//  PhotosViewController.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit
import Lottie
import RealmSwift

class PhotosViewController: UIViewController, UISearchBarDelegate, Loadable {

	// MARK: - IBOutlets
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	// MARK: - ErrorView
	
	lazy var errorView: ErrorView = {
		let nib = UINib(nibName: Constants.errorView, bundle: nil)
		return nib.instantiate(withOwner: nil, options: nil).first as! ErrorView
	}()
	
	// MARK: - SpinnerView
	
	lazy var spinnerView: LOTAnimationView! = {
		let spinner = LOTAnimationView.customSpinner()
		view.addSubview(spinner)
		spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
		spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 50).isActive = true
		return spinner
	}()
	
	// MARK: - Attributes
	
	var flickrService = FlickrService()
	let photoService = PhotoService()
	var photos = [Photo]()
	var selectedIndexPath: IndexPath?
	var isOffline = false
	var selectedTag = Constants.initialSearchTerm {
		didSet {
			title = selectedTag
		}
	}
	
	// MARK: - Paging Attributes
	
	var page = 1
	var totalPages = Int.max
	var isLastPage: Bool {
		return page == totalPages
	}
	
	// MARK: - View Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupNavigatioBar()
		addRefreshControl()
		fetchPhotos()
		collectionView.reloadData()
		title = selectedTag
	}
	
	// MARK: - Customization
	
	func setupNavigatioBar() {
		let searchController = UISearchController(searchResultsController: nil)
		searchController.searchBar.delegate = self
		searchController.searchBar.tintColor = .darkGray
		navigationItem.searchController = searchController
		navigationItem.hidesSearchBarWhenScrolling = false
		navigationItem.rightBarButtonItem?.image = nil
	}
	
	func resetDataSource() {
		page = 1
		photos = []
		isOffline = false
		photoService.cache.removeAllObjects()
		
		DispatchQueue.main.async {
			self.collectionView.reloadData()
			self.navigationItem.rightBarButtonItem?.image = nil
		}
		
		DispatchQueue(label: Constants.backgroundThread).async {
			RealmService.shared.deleteAll()
		}
	}
	
	func addRefreshControl() {
		let refreshControl = UIRefreshControl()
		refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
		
		let loadingView = LOTAnimationView.customPullToRefresh()
		refreshControl.addSubview(loadingView)
		refreshControl.tintColor = .clear
		
		loadingView.centerXAnchor.constraint(equalTo: refreshControl.centerXAnchor).isActive = true
		loadingView.centerYAnchor.constraint(equalTo: refreshControl.centerYAnchor).isActive = true
		loadingView.widthAnchor.constraint(equalToConstant: 150).isActive = true
		loadingView.heightAnchor.constraint(equalToConstant: 150).isActive = true

		collectionView.refreshControl = refreshControl
	}
	
	@objc func refresh(sender: UIRefreshControl) {
		resetDataSource()
		fetchPhotos {
			DispatchQueue.main.async {
				sender.endRefreshing()
			}
		}
	}
	
	func reloadPhotos(forItemsCount totalItems: Int) {
		if self.page != 1 {
			self.collectionView.insertItems(at: self.newIndexPaths(totalItems: totalItems))
		} else {
			self.collectionView.reloadData()
		}
	}
	
	func showError(_ error: ErrorType) {
		DispatchQueue.main.async {
			self.errorView.errorType = error
			self.collectionView.addSubview(self.errorView)
		}
	}
	
	// MARK: - Data Fetcher
	
	func fetchPhotos(completion: (() -> Void)? = nil) {
		if photos.isEmpty { startLoading() }
		flickrService.fetchPhotos(withTags: selectedTag, page: page) {  result in
			completion?()
			self.stopLoading()
			switch result {
			case .success(let result):
				self.handleSuccess(result)
			case .failure(let error):
				self.handleFailure(error)
			}
		}
	}
	
	func handleSuccess(_ result: Photos) {
		totalPages = result.totalPages
		if result.photos.isEmpty {
			showError(.noResults)
		} else {
			photoService.savePhotosToRealm(photos: result.photos)
			photos.append(contentsOf: result.photos)
			DispatchQueue.main.async {
				self.errorView.removeFromSuperview()
				self.reloadPhotos(forItemsCount: result.photos.count)
			}
		}
	}
	
	func handleFailure(_ error: ErrorType) {
		let photosFromRealm = photoService.photosFromRealm()
		if error == .noInternetConnection && !photosFromRealm.isEmpty {
			isOffline = true
			photos = photosFromRealm
			DispatchQueue.main.async {
				self.collectionView.reloadData()
				self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "offiline")
			}
		} else {
			resetDataSource()
			showError(error)
		}
	}
	
	// MARK: - UISearchBarDelegate
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		if let tag = searchBar.text, !tag.isEmpty {
			navigationItem.searchController?.isActive = false
			selectedTag = tag
			resetDataSource()
			fetchPhotos()
		}
	}
	
	// MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == Constants.showDetailsSegue {
			let detailsViewController = segue.destination as! DetailsViewController
			let selectedCell = collectionView.cellForItem(at: selectedIndexPath!) as! PhotoCollectionViewCell
			detailsViewController.image = selectedCell.imageView.image
		}
	}
}
