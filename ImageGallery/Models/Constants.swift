//
//  Constants.swift
//  ImageGallery
//
//  Created by Lucas Domene on 25/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

struct Constants {
	
	// MARK: - Nibs
	
	static let photoCell = "PhotoCell"
	static let errorView = "ErrorView"
	
	// MARK: - Segues
	
	static let showDetailsSegue = "showDetails"
	
	// MARK: - Error messages
	
	static let noInternetConnection = "Check your internet connection"
	static let noResults = "No images to show"
	static let unknownError = "Ops, an error occurred"
	
	// MARK: - Animations
	
	static let noInternetAnimation = "no_internet_connection"
	static let noResultsAnimation = "empty"
	static let unknownErrorAnimation = "unknown_error"
	static let pullToRefreshAnimation = "loader"
	static let spinnerAnimation = "loading"
	
	// MARK: - Realm Keys
	
	static let photoPrimaryKey = "id"
	static let photoImageURL = "imageURL"
	static let photoImageData = "imageData"
	
	// MARK: - Error Codes
	
	static let noInternetError = (-1001, -1009)
	
	// MARK: - Threads
	
	static let backgroundThread = "background"
	
	// MARK: - Parameters Keys
	
	static let methodKey = "method"
	static let tagsKey = "tags"
	static let extrasKey = "extras"
	static let pageNumberKey = "page"
	static let apiKey = "api_key"
	static let responseFormatKey = "format"
	static let noJsonCallbackKey = "nojsoncallback"
	static let itemsPerPageKey = "per_page"
	
	// MARK: - Parameters Values
	
	static let apiKeyValue = "f9cc014fa76b098f9e82f1c288379ea1"
	static let searchPhotosMethod = "flickr.photos.search"
	static let responseFormat = "json"
	static let noJsonCallback = "1"
	static let itemsPerPage = "20"
	static let extras = "url_z"
	
	// MARK: - URLS
	
	static let baseURL = "https://api.flickr.com/services/rest"
	
	// MARK: - Labels
	
	static let initialSearchTerm = "Nature"
	static let detailsViewTitle = "Details"
	
	
}
