//
//  Loading.swift
//  ImageGallery
//
//  Created by Lucas Domene on 24/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit
import Lottie

extension LOTAnimationView {
	
	class func customPullToRefresh() -> LOTAnimationView {
		let animationView = LOTAnimationView(name: Constants.pullToRefreshAnimation)
		setupAnimationView(animationView)
		return animationView
	}
	
	class func customSpinner() -> LOTAnimationView {
		let animationView = LOTAnimationView(name: Constants.spinnerAnimation)
		setupAnimationView(animationView)
		return animationView
	}
	
	class func customError(withName name: String) -> LOTAnimationView {
		let animationView = LOTAnimationView(name: name)
		setupAnimationView(animationView)
		return animationView
	}
	
	private class func setupAnimationView(_ animationView: LOTAnimationView) {
		animationView.play()
		animationView.loopAnimation = true
		animationView.contentMode = .scaleAspectFit
		animationView.translatesAutoresizingMaskIntoConstraints = false
	}
	
}


