//
//  Photo.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import RealmSwift

@objcMembers class Photo: Object, Codable {
	dynamic var id: String = ""
	dynamic var imageURL: String? = ""
	dynamic var imageData: Data? = nil
	
	enum CodingKeys: String, CodingKey {
		case id
		case imageURL = "url_z"
	}
	
	override static func primaryKey() -> String {
		return Constants.photoPrimaryKey
	}
}
