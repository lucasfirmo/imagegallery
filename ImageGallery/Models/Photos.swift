//
//  Photos.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

struct Photos: Codable {
	
	var page: Int
	var totalPages: Int
	var photos: [Photo]
	
	enum CodingKeys: String, CodingKey {
		case page
		case totalPages = "pages"
		case photos = "photo"
	}
	
}

