//
//  ServerResponse.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

struct ServerResponse: Codable {
	
	var photos: Photos
	
}
