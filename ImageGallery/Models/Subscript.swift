//
//  Subscript.swift
//  ImageGallery
//
//  Created by Lucas Domene on 25/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

extension Collection {
	subscript(safe index: Index) -> Element? {
		return indices.contains(index) ? self[index] : nil
	}
}
