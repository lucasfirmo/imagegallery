//
//  ZoomViewTransitionDelegate.swift
//  ImageGallery
//
//  Created by Lucas Domene on 25/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

@objc protocol ZoomingViewController {
	func zoomingImageView(for transition: ZoomTransitionDelegate) -> UIImageView?
}

enum TranstionState {
	case initial
	case final
}

class ZoomTransitionDelegate: NSObject {
	
	var transitionDuration = 0.5
	var operation: UINavigationControllerOperation = .none
	private let zoomScale = CGFloat(15)
	private let backgroundScale = CGFloat(0.7)
	
	typealias ZoomingViews = (otherView: UIView, imageView: UIImageView)
	
	// Transform view according to current navigation operation
	
	func configureView(for state: TranstionState, containerView: UIView, backgroundViewController: UIViewController, viewsInBackground: ZoomingViews, viewsInForeground: ZoomingViews, snapshotViews: ZoomingViews) {
		switch state {
		case .initial:
			backgroundViewController.view.transform = CGAffineTransform.identity
			backgroundViewController.view.alpha = 1
			snapshotViews.imageView.frame = containerView.convert(viewsInBackground.imageView.frame, from: viewsInBackground.imageView.superview)
		case .final:
			backgroundViewController.view.transform = CGAffineTransform(scaleX: backgroundScale, y: backgroundScale)
			backgroundViewController.view.alpha = 0
			snapshotViews.imageView.frame = containerView.convert(viewsInForeground.imageView.frame, from: viewsInForeground.imageView.superview)
		}
	}
	
}

extension ZoomTransitionDelegate: UIViewControllerAnimatedTransitioning {
	
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return transitionDuration
	}
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		let duration = transitionDuration(using: transitionContext)
		let fromViewController = transitionContext.viewController(forKey: .from)!
		let toFromViewController = transitionContext.viewController(forKey: .to)!
		let containerView = transitionContext.containerView
		
		var backgroundViewController = fromViewController
		var foregroundViewController = toFromViewController
		
		// Check which transition will occur, and set the corresponding views
		
		if operation == .pop {
			backgroundViewController = toFromViewController
			foregroundViewController = fromViewController
		}
		
		// Get imageViews from initial and end state
		
		guard let backgroundImageView = (backgroundViewController as? ZoomingViewController)?.zoomingImageView(for: self), let foregroundImageView = (foregroundViewController as? ZoomingViewController)?.zoomingImageView(for: self) else {
			assertionFailure("Cannot find imageView in backgroundViewController or foregroundViewController")
			return
		}
		
		// Create a snapshot of the image that will be animated
		
		let snapshotImageView = UIImageView(image: backgroundImageView.image)
		snapshotImageView.contentMode = .scaleAspectFill
		snapshotImageView.layer.masksToBounds = true
		
		// Hide inital and end imageViews and set the right colors to start the animation
		
		backgroundImageView.isHidden = true
		foregroundImageView.isHidden = true
		
		let foregroundBackgroundColor = foregroundViewController.view.backgroundColor
		foregroundViewController.view.backgroundColor = UIColor.clear
		containerView.backgroundColor = .white
		
		containerView.addSubview(backgroundViewController.view)
		containerView.addSubview(foregroundViewController.view)
		containerView.addSubview(snapshotImageView)
		
		var preTransitionState: TranstionState = .initial
		var posTransitionState: TranstionState = .final
		
		if operation == .pop {
			preTransitionState = .final
			posTransitionState = .initial
		}
		
		// Set the initial state and then run the animation
		
		configureView(for: preTransitionState, containerView: containerView, backgroundViewController: backgroundViewController, viewsInBackground: (backgroundImageView, backgroundImageView), viewsInForeground: (foregroundImageView, foregroundImageView), snapshotViews: (snapshotImageView, snapshotImageView))
		
		foregroundViewController.view.layoutIfNeeded()
		
		UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
			self.configureView(for: posTransitionState, containerView: containerView, backgroundViewController: backgroundViewController, viewsInBackground: (backgroundImageView, backgroundImageView), viewsInForeground: (foregroundImageView, foregroundImageView), snapshotViews: (snapshotImageView, snapshotImageView))
		}) { finished in
			self.finishAnimation(backgroundViewController, foregroundViewController: foregroundViewController, snapshotImageView: snapshotImageView, backgroundImageView: backgroundImageView, foregroundImageView: foregroundImageView, foregroundColor: foregroundBackgroundColor)
			transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
		}
	}
	
	private func finishAnimation(_ backgroundViewController: UIViewController, foregroundViewController: UIViewController, snapshotImageView: UIImageView, backgroundImageView: UIImageView, foregroundImageView: UIImageView, foregroundColor: UIColor?) {
		backgroundViewController.view.transform = CGAffineTransform.identity
		snapshotImageView.removeFromSuperview()
		backgroundImageView.isHidden = false
		foregroundImageView.isHidden = false
		foregroundViewController.view.backgroundColor = foregroundColor
	}
	
}

extension ZoomTransitionDelegate: UINavigationControllerDelegate {
	
	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		if fromVC is ZoomingViewController && toVC is ZoomingViewController {
			self.operation = operation
			return self
		} else {
			return nil
		}
	}
	
}
