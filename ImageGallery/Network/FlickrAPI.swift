//
//  FlickrAPI.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import Foundation

enum FlickrAPI {
	
	// MARK: Endpoints
	
	case search(tags: String, page: Int)

	// MARK: - Parameters
	
	var parameters: [String: Any] {
		switch self {
		case .search(let tags, let page):
			return [Constants.methodKey: Constants.searchPhotosMethod, Constants.tagsKey: tags, Constants.extrasKey: Constants.extras, Constants.pageNumberKey: String(page)]
		}
	}
	
	var requiredParameters: [String: Any] {
		return [Constants.apiKey: Constants.apiKeyValue, Constants.responseFormatKey: Constants.responseFormat, Constants.noJsonCallbackKey: Constants.noJsonCallback, Constants.itemsPerPageKey: Constants.itemsPerPage]
	}
	
	// MARK: - URLRequest Builder
	
	func asURLRequest() -> URLRequest {
		var components = URLComponents(string: Constants.baseURL)
		components?.queryItems = queryItems()
		let urlRequest = URLRequest(url: components?.url ?? URL(string: Constants.baseURL)!)
		return urlRequest
	}
	
	func queryItems() -> [URLQueryItem] {
		let allParameters = parameters.merging(requiredParameters) { current, _ in current }
		return allParameters.map { URLQueryItem(name: $0.key, value: ($0.value as! String)) }
	}
	
}
