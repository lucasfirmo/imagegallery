//
//  HTTPClient.swift
//  ImageGallery
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

class HTTPClient: NSObject, URLSessionDelegate {
	typealias completionClosure = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void
	lazy var session: URLSessionProtocol = {
		let sessionConfig = URLSessionConfiguration.default
		sessionConfig.timeoutIntervalForRequest = 10.0
		return URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
	}()
	var dataTask: URLSessionDataTask?
	
	enum certificatePath: String {
		case flicker = "flickrCertificate"
		case image = "flickrImageCertificate"
		
		init?(domain: String) {
			switch domain {
			case "api.flickr.com":
				self = .flicker
			case "farm1.staticflickr.com":
				self = .image
			default: return nil
			}
		}
	}
	
	func get(urlRequest: URLRequest, completion: @escaping completionClosure) {
		var request = urlRequest
		request.httpMethod = "GET"
		dataTask = session.dataTask(with: request) { data, response, error in
			completion(data, response, error)
		}
		dataTask?.resume()
	}

}
