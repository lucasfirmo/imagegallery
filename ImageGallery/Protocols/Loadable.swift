//
//  Loadable.swift
//  ImageGallery
//
//  Created by Lucas Domene on 24/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit
import Lottie

protocol Loadable {
	var spinnerView: LOTAnimationView! { get }
	func startLoading()
	func stopLoading()
}

extension Loadable {
	func startLoading() {
		DispatchQueue.main.async {
			self.spinnerView.play()
			self.spinnerView.isHidden = false
		}
	}
	
	func stopLoading() {
		DispatchQueue.main.async {
			self.spinnerView.isHidden = true
			self.spinnerView.stop()
		}
	}
}

