//
//  FlickrService.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

enum ErrorType: Error {
	case noInternetConnection
	case fetching
	case parsing
	case unknown
	case noResults
}

enum Result<T> {
	case success(T)
	case failure(ErrorType)
}

final class FlickrService {
	
	var httpClient = HTTPClient()
	
	func fetchPhotos(withTags tags: String, page: Int, completion: @escaping (Result<Photos>) -> Void) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		
		httpClient.get(urlRequest: FlickrAPI.search(tags: tags, page: page).asURLRequest()) { data, response, error in
			DispatchQueue.main.async {
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
			}
			if let error = error {
				let isNetworkError = (error as NSError).code == Constants.noInternetError.0 || (error as NSError).code == Constants.noInternetError.1
				completion(.failure(isNetworkError ? .noInternetConnection : .fetching))
				return
			}
			
			guard let data = data else {
				completion(.failure(.fetching))
				return
			}
			
			let jsonDecoder = JSONDecoder()
			do {
				let serverResponse = try jsonDecoder.decode(ServerResponse.self, from: data)
				completion(.success(serverResponse.photos))
			} catch {
				completion(.failure(.parsing))
			}
		}
	}
	
}
