//
//  PhotoService.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit
import RealmSwift

class PhotoService {
	
	// MARK: - Attributes
	
	let cache = NSCache<AnyObject, AnyObject>()
	var httpClient = HTTPClient()
	
	// MARK: - Downloading Images
	
	func downloadImage(forPhoto photo: Photo, completion: @escaping (UIImage?) -> Void) {
		if let image = imageForKey(photo.id) {
			completion(image)
			return
		} else if let imageURL = photo.imageURL, let url = URL(string: imageURL) {
			httpClient.get(urlRequest: URLRequest(url: url)) { data, response, error in
				completion(data != nil ? UIImage(data: data!) : UIImage(named: "imageViewPlaceholder")!)
			}
		} else {
			completion(UIImage(named: "imageViewPlaceholder")!)
		}
	}
	
	// MARK: - Realm
	
	func photosFromRealm() -> [Photo] {
		let realm = try! Realm()
		return realm.objects(Photo.self).map {
			let photo = Photo()
			photo.id = $0.id
			photo.imageURL = $0.imageURL
			photo.imageData = $0.imageData
			return photo
			}.filter { $0.imageData != nil }
	}
	
	func savePhotosToRealm(photos: [Photo]) {
		for photo in photos {
			DispatchQueue(label: Constants.backgroundThread).async {
				RealmService.shared.save(withType: Photo.self, value: [Constants.photoPrimaryKey: photo.id, Constants.photoImageURL: photo.imageURL])
			}
		}
	}
	
	// MARK: - Persisting images
	
	func setImage(_ image: UIImage?, forKey key: String) {
		if let image = image {
			cache.setObject(image, forKey: key as AnyObject)
			
			if let data = UIImageJPEGRepresentation(image, 0.5) {
				RealmService.shared.save(withType: Photo.self, value: [Constants.photoPrimaryKey: key, Constants.photoImageData: data])
			}
		}
	}
	
	func imageForKey(_ key: String) -> UIImage? {
		if let existingImage = cache.object(forKey: key as AnyObject) as? UIImage {
			return existingImage
		}
		
		let realm = try! Realm()
		guard let photo = realm.object(ofType: Photo.self, forPrimaryKey: key),
			let imageData = photo.imageData,
			let image = UIImage(data: imageData) else {
				return nil
		}
		cache.setObject(image, forKey: key as AnyObject)
		return image
	}
	
}
