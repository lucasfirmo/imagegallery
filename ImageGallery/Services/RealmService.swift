//
//  RealmService.swift
//  ImageGallery
//
//  Created by Lucas Domene on 26/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import Foundation
import RealmSwift

class RealmService {
	
	private init() {}
	static let shared = RealmService()
	
	func save<Element: Object>(withType type: Element.Type, value: Any) {
		let realm = try! Realm()
		realm.beginWrite()
		realm.create(type, value: value, update: true)
		try? realm.commitWrite()
	}
	
	func deleteAll() {
		let realm = try! Realm()
		realm.beginWrite()
		realm.deleteAll()
		try? realm.commitWrite()
	}
	
}
