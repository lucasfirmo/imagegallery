//
//  AppDelegate.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		return true
	}

}

