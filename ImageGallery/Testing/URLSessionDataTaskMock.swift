//
//  URLSessionDataTaskMock.swift
//  ImageGallery
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

final class URLSessionDataTaskMock: URLSessionDataTask {
	var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?
	var taskResponse: (Data?, URLResponse?, Error?)?
	
	override func resume() {
		DispatchQueue.main.async {
			self.completionHandler?(self.taskResponse?.0, self.taskResponse?.1, self.taskResponse?.2)
		}
	}
}
