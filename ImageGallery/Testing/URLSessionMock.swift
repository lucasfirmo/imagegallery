//
//  URLSessionMock.swift
//  ImageGallery
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit

class URLSessionMock: URLSessionProtocol {
	
	var urlRequest: URLRequest?
	private let dataTaskMock: URLSessionDataTaskMock
	
	public init(data: Data? = nil, response: URLResponse? = nil, error: Error? = nil) {
		dataTaskMock = URLSessionDataTaskMock()
		dataTaskMock.taskResponse = (data, response, error)
	}
	
	public func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
		self.urlRequest = request
		self.dataTaskMock.completionHandler = completionHandler
		return self.dataTaskMock
	}
	
}
