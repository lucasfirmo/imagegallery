//
//  ErrorView.swift
//  ImageGallery
//
//  Created by Lucas Domene on 24/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit
import Lottie

class ErrorView: UIView {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var messageLabel: UILabel!
	
	var animationView: LOTAnimationView!
	var errorType: ErrorType! {
		didSet {
			setupView()
		}
	}
	
	var animationName: String {
		switch errorType {
		case .noInternetConnection:
			return Constants.noInternetAnimation
		case .noResults:
			return Constants.noResultsAnimation
		default:
			return Constants.unknownErrorAnimation
		}
	}
	
	var message: String {
		switch errorType {
		case .noInternetConnection:
			return Constants.noInternetConnection
		case .noResults:
			return Constants.noResults
		default:
			return Constants.unknownError
		}
	}
	
	private func setupView() {
		animationView = LOTAnimationView.customError(withName: animationName)
		containerView.addSubview(animationView)
		animationView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
		animationView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
		animationView.widthAnchor.constraint(equalToConstant: 170).isActive = true
		animationView.heightAnchor.constraint(equalToConstant: 170).isActive = true
		messageLabel.text = message
	}
	
}
