//
//  PhotoCollectionViewCell.swift
//  ImageGallery
//
//  Created by Lucas Domene on 23/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import UIKit
import Lottie

class PhotoCollectionViewCell: UICollectionViewCell, Loadable {

	@IBOutlet weak var imageView: UIImageView!
	var spinnerView: LOTAnimationView!
	var id: String!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		addSpinner()
	}
	
	func addSpinner() {
		spinnerView = LOTAnimationView.customSpinner()
		contentView.addSubview(spinnerView)
		spinnerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
		spinnerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
		spinnerView.widthAnchor.constraint(equalToConstant: 170).isActive = true
		spinnerView.heightAnchor.constraint(equalToConstant: 170).isActive = true
	}
}
