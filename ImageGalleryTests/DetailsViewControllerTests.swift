//
//  DetailsViewControllerTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import XCTest
@testable import ImageGallery

class DetailsViewControllerTests: XCTestCase {
	
	var detailsViewController: DetailsViewController!
	
    override func setUp() {
        super.setUp()
		
        detailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
		detailsViewController.image = UIImage(named: "imageViewPlaceholder")
		_ = detailsViewController.view
    }
    
    override func tearDown() {
        detailsViewController = nil
        super.tearDown()
    }
	
	func testIfSetImage() {
		XCTAssertNotNil(detailsViewController.image, "Image should not be nil")
	}
	
	func testZoomingImageView() {
		let zoomTransitionDelegate = ZoomTransitionDelegate()
		let imageView = detailsViewController.zoomingImageView(for: zoomTransitionDelegate)
		XCTAssertEqual(imageView, detailsViewController.imageView)
	}
    
}
