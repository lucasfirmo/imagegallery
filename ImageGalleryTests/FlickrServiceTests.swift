//
//  FlickrServiceTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import XCTest
@testable import ImageGallery

class FlickrServiceTests: XCTestCase {
	
	var httpClient: HTTPClient!
	var session: URLSessionMock!
	var flickrService: FlickrService!
	var data: Data!
	var url: URL!
	
    override func setUp() {
        super.setUp()
		
		let testBundle = Bundle(for: type(of: self))
		let path = testBundle.path(forResource: "photoSearchResponse", ofType: "json")!
		data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
		
		url = URL(string: "https://api.flickr.com/services/rest?page=1&method=flickr.photos.search&format=json&api_key=f9cc014fa76b098f9e82f1c288379ea1&tags=cats&per_page=20&extras=url_q&nojsoncallback=1")!
    }
    
    override func tearDown() {
		session = nil
        httpClient = nil
		flickrService = nil
		data = nil
		url = nil
        super.tearDown()
    }
	
	func configureClient(withData data: Data?, error: Error?) {
		let urlResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
		session = URLSessionMock(data: data, response: urlResponse, error: error)
		httpClient = HTTPClient()
		httpClient.session = session
		flickrService = FlickrService()
		flickrService.httpClient = httpClient
	}
	
	func testSuccessfulFetchPhotosParseData() {
		configureClient(withData: data, error: nil)
		
		let expectation = self.expectation(description: "Status code 200")
		
		flickrService.fetchPhotos(withTags: "testTag", page: 1) { result in
			expectation.fulfill()
			switch result {
			case .success(let photos):
				XCTAssertEqual(photos.photos.count, 20)
			case .failure(let error):
				XCTFail("Error: \(error.localizedDescription)")
			}
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testInternetConnectionError() {
		let noInternetError = NSError(domain: "com.noInternetConnection", code: -1001, userInfo: nil)
		configureClient(withData: data, error: noInternetError)
		
		let expectation = self.expectation(description: "Internet Connection error")
		
		flickrService.fetchPhotos(withTags: "testTag", page: 1) { result in
			expectation.fulfill()
			switch result {
			case .success:
				XCTFail("This test should reproduce an internet connection error")
			case .failure(let error):
				XCTAssertEqual(error, ErrorType.noInternetConnection)
			}
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testFetchingPhotosError() {
		configureClient(withData: nil, error: nil)
		
		let expectation = self.expectation(description: "Internet Connection error")
		
		flickrService.fetchPhotos(withTags: "testTag", page: 1) { result in
			expectation.fulfill()
			switch result {
			case .success:
				XCTFail("This test should reproduce a fetching error")
			case .failure(let error):
				XCTAssertEqual(error, ErrorType.fetching)
			}
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testParsingPhotosError() {
		let testBundle = Bundle(for: type(of: self))
		let path = testBundle.path(forResource: "brokenPhotosResponse", ofType: "json")!
		data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
		configureClient(withData: data, error: nil)
		
		let expectation = self.expectation(description: "Status code 200")
		
		flickrService.fetchPhotos(withTags: "testTag", page: 1) { result in
			expectation.fulfill()
			switch result {
			case .success:
				XCTFail("This test should reproduce a parsing error")
			case .failure(let error):
				XCTAssertEqual(error, ErrorType.parsing)
			}
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}

}
