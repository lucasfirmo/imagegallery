//
//  PhotoServiceTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import XCTest
@testable import ImageGallery

class PhotoServiceTests: XCTestCase {
	
	var httpClient: HTTPClient!
	var session: URLSessionMock!
	var photoService: PhotoService!
	var data: Data!
	
    override func setUp() {
        super.setUp()
		let image = UIImage(named: "imageViewPlaceholder")!
		data = UIImagePNGRepresentation(image)!
    }
    
    override func tearDown() {
        httpClient = nil
		session = nil
		photoService = nil
		data = nil
        super.tearDown()
    }
	
	func configureClient(withData data: Data?, error: Error?) {
		let urlResponse = HTTPURLResponse(url: URL(string: "http://google.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)!
		session = URLSessionMock(data: data, response: urlResponse, error: error)
		httpClient = HTTPClient()
		httpClient.session = session
		photoService = PhotoService()
		photoService.httpClient = httpClient
	}
	
	func testDownloadImage() {
		configureClient(withData: data, error: nil)
		
		let expectation = self.expectation(description: "Download image")
		let photo = Photo()
		photo.id = "1"
		photo.imageURL = "http://path.com"
		
		photoService.downloadImage(forPhoto: photo) { image in
			expectation.fulfill()
			XCTAssertNotNil(image, "Image should not be nil")
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testSaveAndRetrieveImage() {
		configureClient(withData: data, error: nil)
		let image = UIImage(named: "imageViewPlaceholder")
		
		photoService.setImage(image, forKey: "testKey")
		let retrievedImage = photoService.imageForKey("testKey")
		XCTAssertNotNil(retrievedImage, "Image should be fetched")
	}
	
}
