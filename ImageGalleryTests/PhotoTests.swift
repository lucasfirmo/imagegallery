//
//  PhotoTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import XCTest
@testable import ImageGallery

class PhotoTests: XCTestCase {
	
	var serverResponse: ServerResponse?
	
    override func setUp() {
        super.setUp()
		loadResponseFromJSON()
    }
    
    override func tearDown() {
        serverResponse = nil
        super.tearDown()
    }
	
	private func loadResponseFromJSON() {
		let bundle = Bundle(for: type(of: self))
		guard let url = bundle.url(forResource: "photoSearchResponse", withExtension: "json") else {
			XCTFail("Missing file: photoSearchResponse.json")
			return
		}
		
		let json = try! Data(contentsOf: url)
		let jsonDecoder = JSONDecoder()
		serverResponse = try! jsonDecoder.decode(ServerResponse.self, from: json)
	}
    
	func testPhotoParsing() {
		XCTAssertNotNil(serverResponse, "Parsing from json failed")
	}
	
	func testPhotoCount() {
		XCTAssertEqual(serverResponse?.photos.photos.count, 20)
	}
    
}
