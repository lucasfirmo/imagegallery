//
//  PhotosViewControllerTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import XCTest
@testable import ImageGallery

class PhotosViewControllerTests: XCTestCase {
	
	var photosViewController: PhotosViewController!
	var httpClient: HTTPClient!
	var session: URLSessionMock!
	var data: Data!
	var url: URL!
	
    override func setUp() {
        super.setUp()
		
		photosViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
		
		let testBundle = Bundle(for: type(of: self))
		let path = testBundle.path(forResource: "photoSearchResponse", ofType: "json")!
		data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
		
		url = URL(string: "https://api.flickr.com/services/rest?page=1&method=flickr.photos.search&format=json&api_key=f9cc014fa76b098f9e82f1c288379ea1&tags=cats&per_page=20&extras=url_q&nojsoncallback=1")!
		
		_ = photosViewController.view
    }

    override func tearDown() {
		photosViewController.photoService.cache.removeAllObjects()
		RealmService.shared.deleteAll()
        photosViewController = nil
		
        super.tearDown()
    }

	func configureClient(withData data: Data?, error: Error?) {
		let urlResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
		session = URLSessionMock(data: data, response: urlResponse, error: error)
		httpClient = HTTPClient()
		httpClient.session = session
		let flickrService = FlickrService()
		flickrService.httpClient = httpClient
		photosViewController.flickrService = flickrService
	}
	
	func testResetDataSource() {
		photosViewController.resetDataSource()
		XCTAssertEqual(photosViewController.page, 1)
		XCTAssertTrue(photosViewController.photos.isEmpty)
		XCTAssertFalse(photosViewController.isOffline)
	}
	
	func testInitialViewTitle() {
		XCTAssertEqual(Constants.initialSearchTerm, "Nature")
	}
	
	func testFetchPhotos() {
		configureClient(withData: data, error: nil)
		let expectation = self.expectation(description: "Should fetch initial 20 photos")
		
		photosViewController.fetchPhotos {
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
		
		XCTAssertEqual(photosViewController.photos.count, 20)
	}
	
	func testFetchPhotosWithNoResults() {
		let testBundle = Bundle(for: type(of: self))
		let path = testBundle.path(forResource: "photoSearchResponseWithNoResults", ofType: "json")!
		data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
		
		configureClient(withData: data, error: nil)
		let expectation = self.expectation(description: "Should have no results")
		
		photosViewController.fetchPhotos {
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
		
		XCTAssertEqual(photosViewController.photos.count, 0)
	}
	
	func testFetchPhotosWithError() {
		let noInternetError = NSError(domain: "com.noInternetConnection", code: -1001, userInfo: nil)
		configureClient(withData: nil, error: noInternetError)
		
		let expectation = self.expectation(description: "Should have no results")
		
		photosViewController.fetchPhotos {
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
		
		XCTAssertEqual(photosViewController.photos.count, 0)
	}
	
	func testTitleChanged() {
		photosViewController.selectedTag = "Dogs"
		XCTAssertEqual(photosViewController.title, "Dogs")
	}
	
	func testIsLastPage() {
		photosViewController.page = 10
		photosViewController.totalPages = 10
		XCTAssertTrue(photosViewController.isLastPage)
		
		photosViewController.totalPages = 100
		XCTAssertFalse(photosViewController.isLastPage)
	}
	
	func testRefresh() {
		photosViewController.photos = [Photo()]
		photosViewController.refresh(sender: UIRefreshControl())
		XCTAssertTrue(photosViewController.photos.isEmpty)
	}
	
	func testSearchBarSettingSearchTerm() {
		if let searchBar = photosViewController.navigationItem.searchController?.searchBar {
			searchBar.text = "Space"
			photosViewController.searchBarSearchButtonClicked(searchBar)
			XCTAssertEqual(photosViewController.selectedTag, "Space")
		} else {
			XCTFail("Could not get search bar")
		}
	}
	
	func testIndexPathsToReload() {
		for _ in 0..<40 {
			photosViewController.photos.append(Photo())
		}
		
		let indexPaths = photosViewController.newIndexPaths(totalItems: 20)
		XCTAssertEqual(indexPaths.first, IndexPath(row: 20, section: 0))
		XCTAssertEqual(indexPaths.last, IndexPath(row: 39, section: 0))
	}
}
