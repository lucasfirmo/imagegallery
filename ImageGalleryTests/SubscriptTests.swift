//
//  SubscriptTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Domene on 28/07/18.
//  Copyright © 2018 Lucas Domene. All rights reserved.
//

import XCTest
@testable import ImageGallery

class SubscriptTests: XCTestCase {
	
	let stringList = ["first", "second", "third"]
	let intList = [1, 2, 3]
	
	func testSafeSubscription() {
		XCTAssertNotNil(stringList[safe: 2], "Value shouldn't be nil")
		XCTAssertNil(stringList[safe: 3], "Value should be nil")
		XCTAssertNotNil(intList[safe: 2], "Value shouldn't be nil")
		XCTAssertNil(intList[safe: 3], "Value should be nil")
	}
    
}
